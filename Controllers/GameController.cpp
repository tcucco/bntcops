#include "../Components/Point.hpp"
#include "../Models/Area.hpp"
#include "../Models/Character.hpp"
#include "../Models/Traits.hpp"
#include "GameController.hpp"

namespace Controllers
{

using Components::Point;
using Components::Box;
using Models::Area;
using Models::Character;
using Models::Traits;

GameController::GameController()
{
  // TODO: All this static initialization will have to be done through some other
  //       means. This is just for testing purposes.
  Box areaSize(100, 100);
  
  _player = new Character("Trey Cucco", Traits(5, 7, 6, 7, 8, 7, 5), Character::Gender::Male);
  _player->currentPosition(areaSize.center());
  _area = new Area(areaSize, "The Arena!", "The arena is where we test out moving, fighting and NPC AI. We hope to use this as the primary test bed for much of the game engine.");

  _area->npcs().push_back(new Character("Johnny Ringo", Traits(2, 2, 2, 2, 2, 6, 2), Character::Gender::Male));
  _area->npcs().push_back(new Character("Ike Clanton", Traits(3, 3, 3, 3, 3, 6, 3), Character::Gender::Male));
  _area->npcs()[0]->currentPosition(areaSize.center() + Point(areaSize.width() / 10, areaSize.height() / 8));
  _area->npcs()[1]->currentPosition(areaSize.center() + Point(-areaSize.width() / 7, -areaSize.height() / 15));

  double radius = areaSize.width() / 2;
  for (int c = 0; c < areaSize.width(); ++c)
  {
    for (int r = 0; r < areaSize.height(); ++r)
    {
      Point p(c, r);
      if (Components::dist(p, areaSize.center()) > radius)
      {
        _area->location(p).locationType(Models::AreaLocationType::Barrier);
      }
    }
  }
  
  for (auto i = _area->npcs().begin(); i != _area->npcs().end(); ++i)
  {
    _area->location((*i)->currentPosition()).occupyingCharacter(*i);
  }
  _area->location(_player->currentPosition()).occupyingCharacter(_player);
}

GameController::~GameController()
{
  delete _player;
  delete _area;
}

} // namespace Controllers
