#ifndef CONTROLLERS_GAME_CONTROLLER_HPP
#define CONTROLLERS_GAME_CONTROLLER_HPP

#include "../Models/Area.hpp"
#include "../Models/Character.hpp"

namespace Controllers
{

using Models::Area;
using Models::Character;

class GameController
{
public:
  GameController();
  ~GameController();

  Character& player() { return *_player; }
  Area& area() { return *_area; }

private:
  Character* _player;
  Area* _area;

}; // class GameController

} // namespace Controllers

#endif // CONTROLLERS_GAME_CONTROLLER_HPP
