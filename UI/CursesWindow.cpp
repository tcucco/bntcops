#include <string>
#include <algorithm>
#include <cctype>
#include <ncurses.h>
#include "CursesWindow.hpp"

namespace UI
{
namespace Curses
{

using std::string;

// std::list<WINDOW*> Window::_windows;

Window::Window(unsigned int height, unsigned int width, unsigned int top, unsigned int left, string title)
  : _height(height), _width(width), _top(top), _left(left),
    _title(title),
    _lastWrittenLine(0), _lastWrittenColumn(-1),
    _isFocused(false), _isScrollable(false), _isFocusable(true),
    _border(nullptr), _content(nullptr)
{
  _border = newwin(height, width, top, left);
  _content = derwin(_border, innerHeight(), innerWidth(), 1, 1);
  drawBorder();
}

Window::~Window()
{
	wborder(_border, ' ', ' ', ' ',' ',' ',' ',' ',' ');
	wrefresh(_border);
	delwin(_content);
  delwin(_border);
}  

Window& Window::isFocused(bool value)
{
  if (_isFocused != value)
  {
    _isFocused = value;
    drawBorder();
  }
  return *this;
}

Window& Window::isScrollable(bool value)
{
  if (_isScrollable != value)
  {
    _isScrollable = value;
    scrollok(_content, value);
  }
  return *this;
}

Window& Window::isFocusable(bool value)
{
  if (_isFocusable != value)
  {
    _isFocusable = value;
    if (!_isFocusable && isFocused())
    {
      isFocused(false);
    }
  }
  return *this;
}

void Window::drawBorder()
{
  box(_border, 0, 0);
  
  if (title().length() > 0)
  {
    // int pos = (width() - title().length()) / 2 - 1; // Center
    int pos = 1; // Left
    
    if (isFocused())
    {
      wattron(_border, COLOR_PAIR(Window::HighlightTitleColorId));
    }
    
    mvwprintw(_border, 0, pos, "%s", title().c_str());
    
    if (isFocused())
    {
      wattroff(_border, COLOR_PAIR(Window::HighlightTitleColorId));
    }
  }
  
  // TODO: I'm not sure if I want even this refresh here.
  wrefresh(_border);
}

Window& Window::writeLine(string str, string firstLinePrepend, string subsequentLinePrepend)
{
  return writeLine(str, lastWrittenLine() + 1, firstLinePrepend, subsequentLinePrepend);
}
  
Window& Window::writeLine(string str, unsigned int startLine, string firstLinePrepend, string subsequentLinePrepend)
{
  return write(str, startLine, 0, 0, firstLinePrepend, subsequentLinePrepend);
}

Window& Window::write(string str, int attrs, string firstLinePrepend, string subsequentLinePrepend, unsigned int maxLines)
{
  return write(str, lastWrittenLine(), lastWrittenColumn() + 1, attrs, firstLinePrepend, subsequentLinePrepend, maxLines);
}
  
Window& Window::write(string str, unsigned int startLine, unsigned int startColumn, int attrs, string firstLinePrepend, string subsequentLinePrepend, unsigned int maxLines)
{
  if (attrs != 0)
  {
    wattron(_content, attrs);
  }

  maxLines = std::max(1u, maxLines);
  unsigned int linesWritten = 0;
  unsigned int currentLine = std::max(0u, std::min(startLine, innerHeight()));

  while(true)
  {
    // TODO: There's a problem here since we don't check to see if the window
    //       is scrollable. I'm not 100% sure what to do about that right now.
    if (currentLine == innerHeight())
    {
      scrollWindow(ScrollDirection::Up);
      --currentLine;
    }
    
    string* prepend;
    unsigned int column;
    unsigned int maxLength;
    
    if (linesWritten == 0 && startColumn != 0 && startColumn < innerWidth())
    {
      prepend = 0;
      column = std::max(0u, startColumn);
      maxLength = innerWidth() - startColumn;
    }
    else
    {
      prepend = linesWritten == 0u ? &firstLinePrepend : &subsequentLinePrepend;
      column = 0u;
      maxLength = innerWidth() - prepend->length();
    }

    if (str.length() > maxLength)
    {
      int wrapIndex = maxLength - 1;
      
      // If this is not the last line to be written, we'll look for a space to break
      // on. If one is found, we'll break the line on it, otherwise we'll print as
      // much of the string as we can and continue on the next line with the remainder.
      if (linesWritten < maxLines - 1)
      {
        while(wrapIndex >= 0 && !std::isspace(str[wrapIndex]))
        {
          --wrapIndex;
        }
      
        // If we didn't find a space to wrap on, and we didn't start on the first
        // column, we'll continue with the next line.
        if (wrapIndex < 0 && column != 0)
        {
          ++currentLine;
          ++linesWritten;
          continue;
        }
        
        // If no spaces were found, we'll just have to cut the word.
        if (wrapIndex < 0)
        {
          wrapIndex = maxLength - 1;
        }
      }
    
      string printPart = str.substr(0, wrapIndex + 1);
      str.assign(str, wrapIndex + 1, str.length());
      
      mvwprintw(_content, currentLine, column, "%s%s", prepend == 0 ? "" : prepend->c_str(), printPart.c_str());
      ++currentLine;
      ++linesWritten;
      
      if (linesWritten == maxLines)
      {
        break;
      }
    }
    else
    {
      mvwprintw(_content, currentLine, column, "%s%s", prepend == 0 ? "" : prepend->c_str(), str.c_str());
      ++currentLine;
      ++linesWritten;
      break;
    }
  }
  
  getyx(_content, _lastWrittenLine, _lastWrittenColumn);
  --_lastWrittenColumn;
  
  if (attrs != 0)
  {
    wattroff(_content, attrs);
  }
      
  return *this;
}

Window& Window::lineFeed()
{
  ++_lastWrittenLine;
  _lastWrittenColumn = -1;
  return *this;
}

Window& Window::putChar(int c, unsigned int row, unsigned int column, int attrs)
{
  if (attrs)
  {
    wattron(_content, attrs);
  }
  mvwaddch(_content, row, column, c);
  if (attrs)
  {
    wattroff(_content, attrs);
  }
  return *this;
}

Window& Window::clearLine(unsigned int row, unsigned int column)
{
  wmove(_content, row, column);
  wclrtoeol(_content);
  return *this;
}

Window& Window::clear()
{
  wclear(_content);
  _lastWrittenLine = 0;
  _lastWrittenColumn = -1;
  return *this;
}

Window& Window::prepareForUpdate()
{
  wnoutrefresh(_content);
  return *this;
}

Window& Window::update()
{
  // if (full)
  // {
  //   touchwin(_content);
  // }
  wrefresh(_content);
  return *this;
}

Window& Window::scrollWindow(ScrollDirection scrollDirection)
{
  int scrollValue = 1;
  
  if (scrollDirection == ScrollDirection::Down)
  {
    scrollValue = -1;
  }
  
  wscrl(_content, scrollValue);
  
  return *this;
}

} // namespace UI::Curses
} // namespace UI
