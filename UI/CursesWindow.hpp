#ifndef UI_CURSES_WINDOW_HPP
#define UI_CURSES_WINDOW_HPP

#include <string>
#include <climits>
#include <ncurses.h>

namespace UI
{
namespace Curses
{

using std::string;
  
enum class ScrollDirection { Up, Down };

class Window
{
public:
  Window(unsigned int height, unsigned int width, unsigned int top, unsigned int left, string title);
  ~Window();

  // Read Only Properties
  inline unsigned int height() const { return _height; }
  inline unsigned int width() const { return _width; }
  inline unsigned int top() const { return _top; }
  inline unsigned int left() const { return _left; }
  inline unsigned int innerHeight() const { return _height - 2; }
  inline unsigned int innerWidth() const { return _width - 2; }
  inline unsigned int innerTop() const { return _top + 1; }
  inline unsigned int innerLeft() const { return _left + 1; }
  inline const string& title() const { return _title; }
  inline int lastWrittenLine() const { return _lastWrittenLine; }
  inline int lastWrittenColumn() const { return _lastWrittenColumn; }
  
  // Read/Write Properties
  inline bool isFocused() const { return _isFocused; }
  Window& isFocused(bool value);

  inline bool isScrollable() const { return _isScrollable; }
  Window& isScrollable(bool value);
  
  inline bool isFocusable() const { return _isFocusable; }
  Window& isFocusable(bool value);
  
  // Writing Methods
  Window& writeLine(string str, string firstLinePrepend = "", string subsequentLinePrepend = "");
  Window& writeLine(string str, unsigned int startLine, string firstLinePrepend = "", string subsequentLinePrepend = "");
  Window& write(string str, int attrs = 0, string firstLinePrepend = "", string subsequentLinePrepend = "", unsigned int maxLines = UINT_MAX);
  Window& write(string str, unsigned int startLine, unsigned int startColumn, int attrs = 0, string firstLinePrepend = "", string subsequentLinePrepend = "", unsigned int maxLines = UINT_MAX);
  Window& lineFeed();
  Window& putChar(int c, unsigned int row, unsigned int column, int attrs = 0);
  
  // Clearing Methods
  Window& clearLine(unsigned int row, unsigned int column = 0u);
  Window& clear();
  
  // Other Methods
  Window& prepareForUpdate();
  Window& update();
  Window& scrollWindow(ScrollDirection scrollDirection);
  
  static const int HighlightTitleColorId = 10;

private:
  void drawBorder();

  unsigned int _height;
  unsigned int _width;
  unsigned int _top;
  unsigned int _left;
  string _title;
  unsigned int _lastWrittenLine;
  unsigned int _lastWrittenColumn;
  bool _isFocused;
  bool _isScrollable;
  bool _isFocusable;
  WINDOW* _border;
  WINDOW* _content;
}; // class Window

} // namespace UI::Curses
} // namespace UI

#endif // UI_CURSES_WINDOW_H
