#include <complex>
#include "Point.hpp"

namespace Components
{

using std::complex;

Point::Point(double x, double y)
  : complex<double>(x, y)
{ }

Point::Point(complex<double> c)
  : complex<double>(c.real(), c.imag())
{ }

Point::Point(const Point& p)
  : complex<double>(p.x(), p.y())
{ }

Point unit(const Point& p)
{
  return p / std::abs(p);
}

double dist(const Point& from, const Point& to)
{
  return std::abs(to - from);
}

Point round(const Point& p)
{
  return Point(std::floor(p.x() + 0.5), std::floor(p.y() + 0.5));
}

Point ceil(const Point& p)
{
  return Point(std::ceil(p.x()), std::ceil(p.y()));
}

Point floor(const Point& p)
{
  return Point(std::floor(p.x()), std::floor(p.y()));
}

} // namespace Components
