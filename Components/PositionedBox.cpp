#include "Box.hpp"
#include "Point.hpp"
#include "PositionedBox.hpp"

namespace Components
{

PositionedBox::PositionedBox(double width, double height, double topLeftX, double topLeftY)
  : Box(width, height), _topLeft(topLeftX, topLeftY)
{ }

PositionedBox::PositionedBox(double width, double height, const Point& topLeft)
  : Box(width, height), _topLeft(topLeft)
{ }

PositionedBox::PositionedBox(const Box& box, const Point& topLeft)
  : Box(box), _topLeft(topLeft)
{ }

PositionedBox::PositionedBox(const Box& box, double topLeftX, double topLeftY)
  : Box(box), _topLeft(topLeftX, topLeftY)
{ }

PositionedBox::PositionedBox(const PositionedBox& positionedBox)
  : Box(positionedBox.width(), positionedBox.height()), _topLeft(positionedBox.topLeft())
{ }

PositionedBox::~PositionedBox()
{ }

bool PositionedBox::containsPoint(const Point& p) const
{
  return Box::containsPoint(p - topLeft());
}

} // namespace Components
