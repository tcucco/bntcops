#ifndef COMPONENTS_POINT_HPP
#define COMPONENTS_POINT_HPP

#include <complex>

namespace Components
{

using std::complex;

class Point : public complex<double>
{
public:
  Point(double x = 0.0, double y = 0.0);
  Point(complex<double> c);
  Point(const Point& p);

  double x() const { return complex<double>::real(); }
  double y() const { return complex<double>::imag(); }
}; // class Point

Point unit(const Point& p);
double dist(const Point& from, const Point& to);
Point round(const Point& p);
Point ceil(const Point& p);
Point floor(const Point& p);

} // namespace Components

#endif // COMPONETS_POINT_HPP
