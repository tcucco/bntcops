#include <algorithm>
#include "Point.hpp"
#include "Box.hpp"

namespace Components
{

Box::Box(double width, double height)
  : _width(std::max(width, 0.0)), _height(std::max(height, 0.0))
{ }

Box::Box(const Box& box)
  : _width(std::max(box.width(), 0.0)), _height(std::max(box.height(), 0.0))
{ }

Box::~Box()
{ }

bool Box::containsPoint(const Point& p) const
{
  return p.x() >= 0 && p.y() >= 0 && p.x() < width() && p.y() < height();
}

} // namespace Components
