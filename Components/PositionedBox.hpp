#ifndef COMPONENTS_POSITIONED_BOX_HPP
#define COMPONENTS_POSITIONED_BOX_HPP

#include "Box.hpp"
#include "Point.hpp"

namespace Components
{

class PositionedBox : public Box
{
public:
  PositionedBox(double width = 0.0, double height = 0.0, double topLeftX = 0.0, double topLeftY = 0.0);
  PositionedBox(double width, double height, const Point& topLeft);
  PositionedBox(const Box& box, double topLeftX, double topLeftY);
  PositionedBox(const Box& box, const Point& topLeft);
  PositionedBox(const PositionedBox& positionedBox);
  virtual ~PositionedBox();

  virtual bool containsPoint(const Point& p) const;

  inline virtual const Point center() const { return Box::center() + topLeft(); }
  inline virtual const Point topLeft() const { return _topLeft; }
  inline virtual const Point topRight() const { return _topLeft + Box::topRight(); }
  inline virtual const Point bottomLeft() const { return _topLeft + Box::bottomLeft(); }
  inline virtual const Point bottomRight() const { return _topLeft + Box::bottomRight(); }

private:
  Point _topLeft;

}; // class Positioned Box

} // namespace Components

#endif // COMPONENTS_POSITIONED_BOX_HPP
