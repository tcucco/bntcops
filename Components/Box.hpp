#ifndef COMPONENTS_BOX_HPP
#define COMPONENTS_BOX_HPP

#include "Point.hpp"

namespace Components
{

class Box
{
public:
  Box(double width = 0, double height = 0);
  Box(const Box& box);
  virtual ~Box();

  inline double width() const { return _width; }
  inline double height() const { return _height; }
  inline double area() const { return _width * _height; }
  virtual bool containsPoint(const Point& p) const;

  inline virtual const Point center() const { return Point(width() / 2, height() / 2); }
  inline virtual const Point topLeft() const { return Point(0, 0); }
  inline virtual const Point topRight() const { return Point(width(), 0); }
  inline virtual const Point bottomLeft() const { return Point(0, height()); }
  inline virtual const Point bottomRight() const { return Point(width(), height()); }

private:
  double _width;
  double _height;

}; // class Box

} // namespace Components

#endif // COMPONENTS_BOX_HPP
