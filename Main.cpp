#include <iostream>
#include "Exceptions/GameException.hpp"
#include "Controllers/GameController.hpp"
#include "Views/MainView.hpp"

int main(int argc, char** argv)
{
  try
  {
    Controllers::GameController gameController;
    Views::MainView interface(gameController);
    interface.commandLoop();
  }
  catch (Exceptions::GameException& ex)
  {
    std::cout << "An exception was thrown: " << ex.description() << std::endl;
  }
}
