#ifndef EXCEPTIONS_GAME_EXCEPTION_HPP
#define EXCEPTIONS_GAME_EXCEPTION_HPP

#include <string>
#include <exception>

namespace Exceptions
{

using std::string;

// This is the base class for all exceptions in the game.
class GameException : public std::exception
{
public:
  explicit GameException(const string& description) throw();
  virtual ~GameException() throw();
  
  inline virtual const char* what() const throw() { return description().c_str(); }
  inline const string& description() const { return _description; }

private:
  string _description;

}; // class GameException

} // namespace Exceptions

#endif // EXCEPTIONS_GAME_EXCEPTION_HPP
