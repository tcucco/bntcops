#include <string>
#include <exception>
#include "GameException.hpp"

namespace Exceptions
{

using std::string;

GameException::GameException(const string& description) throw()
  : _description(description)
{ }

GameException::~GameException() throw()
{ }

} // namespace Exceptions
