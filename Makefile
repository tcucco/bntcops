include Makefile.common

BUILD_OBJECTS = Main.o
LINK_OBJECTS = UI/UI.o Components/Components.o Exceptions/Exceptions.o Models/Models.o Views/Views.o Controllers/Controllers.o Helpers/Helpers.o
OUTPUT = Game.exe

# I'd like to figure out some way of just listing off the
# link_objects as above, and then having make automaticall
# build and clean those directories without having to list
# out the cd X; make and cd X; make clean in the recipes
# below. --Trey 9/27/2012

all: $(BUILD_OBJECTS)
	cd Models; make
	cd Views; make
	cd Controllers; make
	cd UI; make
	cd Components; make
	cd Exceptions; make
	cd Helpers; make
	$(CXX) -o $(OUTPUT) $(BUILD_OBJECTS) $(LINK_OBJECTS) -lcurses  -L"/usr/local/lib" -lboost_signals -lboost_thread

Main.o: Main.cpp \
	Controllers/GameController.hpp \
	Views/MainView.hpp

# UI/UI.o:
# 	cd UI; make
# 
# Components/Components.o:
# 	cd Components; make
# 
# Helpers/Helpers.o:
# 	cd Helpers; make
# 
# Models/Models.o:
# 	cd Models; make
# 
# Views/Views.o:
# 	cd Views; make
# 
# Controllers/Controllers.o:
# 	cd Controllers; make

clean:
	cd UI; make clean
	cd Components; make clean
	cd Exceptions; make clean
	cd Helpers; make clean
	cd Models; make clean
	cd Views; make clean
	cd Controllers; make clean
	rm *.o $(OUTPUT)
