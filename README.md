[Bacon!] Ninja (Time?) Cops

NOTE:
This project was started as a joke and an opportunity to learn some
technologies that I don't get to use on a regular basis. After getting it
working up to a point I abandoned the project.

As you can maybe guess, the name of this game is a joke that resulted in an
escalating ridiculousness back and forth, and once we got to a certain point
we decided to actually use the name.

We're designing the game to be a turn-based action/RPG, similar to Fallout 1
and 2 in mechanics.

We're primarily working on this game to play with C++11, Curses and Boost. As
such, you'll need:

1. The Curses Library installed
2. Boost C++ libraries installed
3. A C++ compiler that supports the same C++11 features as GCC 4.6
