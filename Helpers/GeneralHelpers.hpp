#ifndef HELPERS_GENERAL_HELPERS_HPP
#define HELPERS_GENERAL_HELPERS_HPP

#include <string>
#include <sstream>

namespace Helpers
{

using std::string;

template <class T>
string toString(T t)
{
  std::stringstream ss;
  ss << t;
  return ss.str();
}

template <class T>
T getRangeBoundValue(T value, T min, T max)
{
  if (value < min) { return min; }
  if (value > max) { return max; }
  return value;
}

} // namespace Helpers

#endif // HELPERS_GENERAL_HELPERS_HPP
