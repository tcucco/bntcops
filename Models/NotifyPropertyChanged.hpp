#ifndef MODELS_NOTIFY_PROPERTY_CHANGED_HPP
#define MODELS_NOTIFY_PROPERTY_CHANGED_HPP

#include <boost/signals.hpp>

namespace Models
{

template <class ModelType, class PropertyIdentifyingType>
class NotifyPropertyChanged
{
public:
  typedef boost::signal<void (ModelType, PropertyIdentifyingType)> PropertyChangedSignalType;

  PropertyChangedSignalType& propertyChanged() { return _propertyChanged; }

private:
  PropertyChangedSignalType _propertyChanged;

}; // class NotifyPropertyChanged

} // namespace Models

#endif // MODELS_NOTIFY_PROPERTY_CHANGED_HPP
