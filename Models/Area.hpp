#ifndef MODELS_AREA_HPP
#define MODELS_AREA_HPP

#include <string>
#include <valarray>
#include <vector>
#include "../Components/Point.hpp"
#include "../Components/Box.hpp"
#include "Character.hpp"
#include "boost/signals.hpp"

namespace Models
{

using std::string;
using std::valarray;
using Components::Point;
using Components::Box;
using Models::Character;

enum class AreaLocationType { Floor, Barrier, Exit };

string toString(AreaLocationType alt);

// AreaLocation encapsulates all the information about a particular location
// in an area.
class AreaLocation
{
public:
  AreaLocation(AreaLocationType locationType = AreaLocationType::Floor);
  ~AreaLocation();

  // NOTE: I'm allowing the type to be set after object creation for now
  //       because I'm not sure the best way to set it up, and how
  //       deserialization will work. I'll either turn this off, or only allow
  //       it to be set once, because (at least at the moment) I don't like the
  //       idea of the type of a location being changable.
  inline AreaLocationType locationType() const { return _locationType; }
  AreaLocation& locationType(AreaLocationType locationType);
  
  inline Character* occupyingCharacter() const { return _occupyingCharacter; }
  AreaLocation& occupyingCharacter(Character* character);

  inline bool isOccupied() const { return occupyingCharacter() != nullptr; }
  inline bool canBeOccupied() const { return locationType() != AreaLocationType::Barrier; }

private:
  AreaLocationType _locationType;
  Character* _occupyingCharacter;
  // We'll eventually add a vector<Item*> for items dropped on the spot, perhaps.
}; // class AreaLocation

class Area
{
public:
  typedef boost::signal<void (const Area& area, const Character& character, const Point& from, const Point& to)> CharacterMovedSignal;
  typedef std::vector<Character*> NpcList;

  Area(int width, int height, const string& name, const string& description);
  Area(const Box& size, const string& name, const string& description);
  ~Area();

  AreaLocation& location(int x, int y);
  inline AreaLocation& location(const Point& p) { return location(p.x(), p.y()); }
  inline const AreaLocation& location(int x, int y) const { return location(x, y); }
  inline const AreaLocation& location(const Point& p) const { return location(p); }

  inline int width() const { return _width; }
  inline int height() const { return _height; }
  inline const string& name() const { return _name; }
  inline const string& description() const { return _description; }
  inline NpcList& npcs() { return _npcs; }

  inline CharacterMovedSignal& characterMoved() { return _characterMoved; }

  Area& moveCharacter(Character& from, const Point& to);

private:
  int _width;
  int _height;
  string _name;
  string _description;
  valarray<AreaLocation> _locations;
  CharacterMovedSignal _characterMoved;
  NpcList _npcs;
}; // class Area

} // namespace Models

#endif // MODELS_AREA_HPP
