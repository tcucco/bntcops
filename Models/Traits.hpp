#ifndef MODELS_TRAITS_HPP
#define MODELS_TRAITS_HPP

namespace Models
{

// Traits are the foundational aspects of characters from which all other stats are determined.
class Traits
{
public:
	Traits(int strength, int perception, int endurance, int charisma, int intelligence, int agility, int luck);

	inline int strength() const { return _strength; }
	inline int perception() const { return _perception; }
	inline int endurance() const { return _endurance; }
	inline int charisma() const { return _charisma; }
	inline int intelligence() const { return _intelligence; }
	inline int agility() const { return _agility; }
	inline int luck() const { return _luck; }

	static const int MinimumValue = 1;
	static const int MaximumValue = 10;

private:
	static int GetRangeBoundValue(int value);

	int _strength;
	int _perception;
	int _endurance;
	int _charisma;
	int _intelligence;
	int _agility;
	int _luck;
}; // class Traits

} // namespace Models

#endif // MODELS_TRAITS_HPP
