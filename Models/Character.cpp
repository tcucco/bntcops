#include <string>
#include <algorithm>
#include "../Exceptions/GameException.hpp"
#include "Character.hpp"

namespace Models
{

using std::string;
using Components::Point;

unsigned int Character::NextId = 0;

Character::Character(const string& name, Traits traits, Gender gender)
  : _id(Character::NextId++), _name(name), 
    _traits(traits), _gender(gender),
    _maximumHitPoints(0), _currentHitPoints(0), 
    _maximumCarryWeight(0), _currentCarryWeight(0),
    _maximumActionPoints(0), _currentActionPoints(0)
{
	updateStats();
}

Character& Character::currentPosition(const Point& position)
{
  _currentPosition = position;
  return *this;
}

void Character::useActionPoints(unsigned int ap)
{
  if (_currentActionPoints < ap)
  {
    throw Exceptions::GameException("Insufficient action points.");
  }
  else
  {
    _currentActionPoints -= ap;
    propertyChanged()(*this, CharacterProperty::CurrentActionPoints);
  }
}

void Character::refreshActionPoints()
{
  _currentActionPoints = maximumActionPoints();
  propertyChanged()(*this, CharacterProperty::CurrentActionPoints);
}

void Character::attacked(unsigned int dmg)
{
  // Eventually we'll add damage type and consider dodging, AC, etc.
  _currentHitPoints = std::max(_currentHitPoints - dmg, 0u);
  propertyChanged()(*this, CharacterProperty::CurrentHitPoints);
}

void Character::healed(unsigned int amount)
{
  _currentHitPoints = std::min(_maximumHitPoints, _currentHitPoints + amount);
  propertyChanged()(*this, CharacterProperty::CurrentHitPoints);
}

unsigned int Character::maxMoveDistance()
{
  return currentActionPoints();
}

bool Character::canMoveDistance(unsigned int distance)
{
  return currentActionPoints() >= distance;
}

Character& Character::movedDistance(unsigned int distance)
{
  useActionPoints(distance);
  return *this;
}

void Character::updateStats()
{
	_maximumHitPoints = 50 + traits().endurance() * 5 + traits().strength() * 2;
	_maximumCarryWeight = 50 + traits().strength() * 10;
	_maximumActionPoints = 5 + std::max(0, traits().agility() - 5);
  refreshActionPoints();
  healed(_maximumHitPoints);
}

bool operator==(const Character& lhs, const Character& rhs)
{
  return lhs.id() == rhs.id();
}

bool operator!=(const Character& lhs, const Character& rhs)
{
  return !(lhs == rhs);
}

string getPronoun(Character::Gender gender, bool capitalize, bool plural, bool possessive)
{
  switch(gender)
  {
    case Character::Gender::Male:
      if (plural)
      {
        if (possessive)
        {
          return capitalize ? "Theirs" : "theirs";
        }
        else
        {
          return capitalize ? "They" : "they";
        }
      }
      else
      {
        if (possessive)
        {
          return capitalize ? "His" : "his";
        }
        else
        {
          return capitalize ? "He" : "he";
        }
      }
      break;
    case Character::Gender::Female:
      if (plural)
      {
        if (possessive)
        {
          return capitalize ? "Theirs" : "theirs";
        }
        else
        {
          return capitalize ? "They" : "they";
        }
      }
      else
      {
        if (possessive)
        {
          return capitalize ? "Hers" : "hers";
        }
        else
        {
          return capitalize ? "She" : "she";
        }
      }
      break;
    case Character::Gender::Unspecified:
    default:
      if (plural)
      {
        if (possessive)
        {
          return capitalize ? "Theirs" : "theirs";
        }
        else
        {
          return capitalize ? "They" : "they";
        }
      }
      else
      {
        if (possessive)
        {
          return capitalize ? "Theirs" : "theirs";
        }
        else
        {
          return capitalize ? "They" : "they";
        }
      }
      break;
  }
}

} // namespace Models
