#include <string>
#include <valarray>
#include <cmath>
#include <algorithm>
#include "../Components/Point.hpp"
#include "../Components/Box.hpp"
#include "../Exceptions/GameException.hpp"
#include "Character.hpp"
#include "Area.hpp"

namespace Models
{

using std::string;
using std::valarray;
using Components::Point;
using Components::Box;

string toString(AreaLocationType alt)
{
  switch(alt)
  {
    case AreaLocationType::Floor:
      return "Floor";
    case AreaLocationType::Barrier:
      return "Barrier";
    case AreaLocationType::Exit:
      return "Exit";
  }
  return "Unknown";
}

// class AreaLocation members
AreaLocation::AreaLocation(AreaLocationType locationType)
  : _locationType(locationType),
    _occupyingCharacter(nullptr)
{ }

AreaLocation::~AreaLocation()
{ }

AreaLocation& AreaLocation::locationType(AreaLocationType locationType)
{
  _locationType = locationType;
  return *this;
}

AreaLocation& AreaLocation::occupyingCharacter(Character* character)
{
  // TODO: Should we first make sure that no character is on the
  //       position, and if so throw an exception?
  if (_occupyingCharacter != character)
  {
    _occupyingCharacter = character;
  }
  return *this;
}

// class AreaMembers
Area::Area(int width, int height, const string& name, const string& description)
  : _width(width),
    _height(height),
    _name(name),
    _description(description),
    _locations(width * height)
{ }

Area::Area(const Box& size, const string& name, const string& description)
  : _width(size.width()), 
    _height(size.height()), 
    _name(name), 
    _description(description),
    _locations(size.width() * size.height())
{ }

Area::~Area()
{
  for (auto i = npcs().begin(); i != npcs().end(); ++i)
  {
    delete *i;
  }
}

AreaLocation& Area::location(int x, int y)
{
  if (x < 0 || x >= _width || y < 0 || y >= _height)
  {
    throw Exceptions::GameException("Given coordinates are outside the bounds of the Area.");
  }

  return _locations[x + y * _width];
}

Area& Area::moveCharacter(Character& character, const Point& to)
{
  AreaLocation& startLocation = location(character.currentPosition());

  if (!startLocation.isOccupied() || *startLocation.occupyingCharacter() != character)
  {
    throw Exceptions::GameException("The given character isn't registered with the area in the location they claim to be.");
  }

  Point from = character.currentPosition();
  double totalDistance = Components::dist(to, from);
  
  // TODO: Perhaps we should have a fudge factor here. Say 0.001 or something?
  if (totalDistance > 0)
  {
    // TODO: This needs to somehow take away AP when in combat mode. I'm not sure
    //       if I'll do that through different types of move calls on character,
    //       or what. But during non-combat mode, the below will work properly.
    // TODO: Put this in its own thread so the user can continue to move the
    //       cursor and interact with the UI when characters are moving around.
    
    Point currentPosition = from;
    Point directionVector = Components::unit(to - from);
    int maxSteps = std::ceil(totalDistance);
    // TODO: Perhaps maxStepDistance can be an attribute of an each character
    //       and things like character height, or perhaps agility can alter
    //       that value some, causing them to move more quickly. For now, we'll
    //       make it a value that is set here.
    int maxStepDistance = 1.0;
    int stepsTaken;
    
    for (stepsTaken = 0; stepsTaken < maxSteps; ++stepsTaken)
    {
      // In the case that the distance traveled is not a whole number, the
      // last step will need to be a fractional distance. So for instance:
      // totalDistance = 8.5
      // => maxSteps = 9
      // => on step 8, totalDistance - maxStepDistance = 8.5 - 1.0
      // => next distance = totalDistance - stepsTaken = 8.5 - 8.0 = 0.5
      double nextDistance = stepsTaken > totalDistance - maxStepDistance ? totalDistance - stepsTaken: maxStepDistance;
      Point nextPosition = currentPosition + directionVector * nextDistance;

      AreaLocation& currentLocation = location(currentPosition);
      AreaLocation& nextLocation = location(nextPosition);

      // Make sure that the next area can be occupied and is not occupied by
      // a different character. In some instances the currentLocation and
      // nextLocation can be the same, if moving the character by a distance
      // of 1 does not cause them to leave that location.
      if (&currentLocation == &nextLocation || (nextLocation.canBeOccupied() && !nextLocation.isOccupied()))
      {
        character.currentPosition(nextPosition);
        
        if (&currentLocation != &nextLocation)
        {
          currentLocation.occupyingCharacter(nullptr);
          nextLocation.occupyingCharacter(&character);
        }
        
        characterMoved()(*this, character, currentPosition, nextPosition);
        currentPosition = nextPosition;
      }
      else
      {
        break;
      }
    }
    // TODO: Reduce character AP accordingly for how many steps were taken.
    //       This should be done in such a way that the steps taken are
    //       given to the character, and they decide how many AP that cost.
  }
  return *this;
}

} // namespace Models
