#ifndef MODELS_CHARACTER_HPP
#define MODELS_CHARACTER_HPP

#include <string>
#include "../Components/Point.hpp"
#include "NotifyPropertyChanged.hpp"
#include "Traits.hpp"

namespace Models
{

using std::string;
using Components::Point;

enum class CharacterProperty
{
  Traits,
  MaximumHitPoints, CurrentHitPoints,
  MaximumCarryWeight, CurrentCarryWeight,
  MaximumActionPoints, CurrentActionPoints,
  CurrentPosition
};

class Character : public NotifyPropertyChanged<const Character&, CharacterProperty>
{
public:
  enum class Gender { Male, Female, Unspecified };

	Character(const string& name, Traits traits, Gender gender);

  inline unsigned int id() const { return _id; }
	inline const string& name() const { return _name; }
	inline const Traits& traits() const { return _traits; }
  inline Gender gender() const { return _gender; }

	inline unsigned int maximumHitPoints() const { return _maximumHitPoints; }
	inline unsigned int currentHitPoints() const { return _currentHitPoints; }
	inline unsigned int maximumCarryWeight() const { return _maximumCarryWeight; }
	inline unsigned int currentCarryWeight() const { return _currentCarryWeight; }
	inline unsigned int maximumActionPoints() const { return _maximumActionPoints; }
	inline unsigned int currentActionPoints() const { return _currentActionPoints; }
	inline bool isDead() const { return _currentHitPoints == 0; }

  inline const Point& currentPosition() const { return _currentPosition; }
  Character& currentPosition(const Point& newPosition);

  void useActionPoints(unsigned int ap);
  void refreshActionPoints();
  void attacked(unsigned int dmg);
  void healed(unsigned int amount);

  unsigned int maxMoveDistance();
  bool canMoveDistance(unsigned int distance);
  Character& movedDistance(unsigned int distance);

private:
  unsigned int _id;
	string _name;
	Traits _traits;
  Gender _gender;
	unsigned int _maximumHitPoints;
	unsigned int _currentHitPoints;
	unsigned int _maximumCarryWeight;
	unsigned int _currentCarryWeight;
	unsigned int _maximumActionPoints;
	unsigned int _currentActionPoints;
  Point _currentPosition;

  static unsigned int NextId;

	void updateStats();
}; // class Character

bool operator==(const Character& lhs, const Character& rhs);
bool operator!=(const Character& lhs, const Character& rhs);

string getPronoun(Character::Gender gender, bool capitalize = false, bool plural = false, bool possessive = false);

} // namespace Models

#endif // MODELS_CHARACTER_HPP
