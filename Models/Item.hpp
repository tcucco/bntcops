#ifndef MODELS_ITEM_HPP
#define MODELS_ITEM_HPP

#include <string>
#include <memory>
#include <vector>
#include <functional>

// NOTE: I've decided to put all of the Item, ItemAction and
//       ItemActionExecutionArg classes in this one file. I think that since
//       they are all so closesly related and depend on each other, this is OK.
namespace Models
{

using std::string;

class Character;
class Area;
class ItemAction;


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
// Items
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

// The ItemCore class encapsulates the data that is common across all items and
// will not change from instance to instance. It is shared among Items of the
// same type by a std::shared_ptr, so that there is only one instance of each
// core, no matter how many items use it.
class ItemCore
{
public:
  ItemCore(unsigned int id, const string& name, const string& description, unsigned int weight, unsigned int value);
  ~ItemCore();
  
  inline unsigned int id() const { return _id; }
  inline const string& name() const { return _name; }
  inline const string& description() const { return _description; }
  inline unsigned int weight() const { return _weight; }
  inline unsigned int value() const { return _value; }

private:
  unsigned int _id;
  string _name;
  string _description;
  unsigned int _weight;
  unsigned int _value;
}; // class ItemCore

bool operator==(const ItemCore& lhs, const ItemCore& rhs);


// The Item class is the base class for all Items.
class Item
{
public:
  typedef std::vector<ItemAction*> ActionCollection;
  
  Item(ItemCore* itemCore);
  Item(const Item& item);
  virtual ~Item();
  
  inline unsigned int id() const { return _core->id(); }
  inline const string& name() const { return _core->name(); }
  inline const string& description() const { return _core->description(); }
  inline unsigned int weight() const { return _core->weight(); }
  inline unsigned int value() const { return _core->value(); }

  // TODO: I may add a member like noMoreActions() to cause addAction to fail.
  //       This way it can be guaranteed that items will not gain additional
  //       actions after creation.
  Item& addAction(ItemAction* action);
  
  virtual bool equals(const Item& other) const;
    
  ActionCollection::iterator actionsBegin() { return _actions.begin(); }
  ActionCollection::iterator actionsEnd() { return _actions.end(); }

private:
  std::shared_ptr<ItemCore> _core;
  ActionCollection _actions;
}; // class Item

bool operator==(const Item& lhs, const Item& rhs);


class Ammunition : public Item
{
public:
  enum class Type
  {
    a9mm,
    a45,
    a223,
    a762,
    aShotgun,
    aArrow
  };
  
  Ammunition(ItemCore* itemCore, Type type);
  virtual ~Ammunition();
  
  inline Type type() const { return _type; }

  // TODO: Do I need to override equals to include type?
  // virtual bool equals(const Item& other);

private:
  Type _type;

}; // class Ammunition


class AmmunitionConsumingWeapon : Item
{
public:
  AmmunitionConsumingWeapon(ItemCore* itemCore, Ammunition::Type ammunitionType, unsigned int ammunitionCapacity, unsigned int ammunitionCount);
  ~AmmunitionConsumingWeapon();
  
  inline Ammunition::Type ammunitionType() const { return _ammunitionType; }
  inline unsigned int ammunitionCapacity() const { return _ammunitionCapacity; }
  inline unsigned int ammunitionCount() const { return _ammunitionCapacity; }

  AmmunitionConsumingWeapon& useAmmunition(unsigned int amount);
  
  virtual bool equals(const Item& other);

private:
  Ammunition::Type _ammunitionType;
  unsigned int _ammunitionCapacity;
  unsigned int _ammunitionCount;  
}; // AmmunitionConsumingWeapon


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
// Item Action Execution Arg
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

// The ItemActionExecutionArg class is used during the ItemAction execution
// cycle. It holds all the data an action needs to determine if it can execute,
// and everything it needs to update when it does execute.
class ItemActionExecutionArg
{
public:
  typedef std::function<void (const string&)> Reporter;
  
  ItemActionExecutionArg(Character& byCharacter, Character* onCharacter, Item& withItem, Area& inArea, Reporter reporter = nullptr);
  
  inline Character& byCharacter() { return _byCharacter; }
  inline Character* onCharacter() { return _onCharacter; }
  inline Item& withItem() { return _withItem; }
  inline Area& inArea() { return _inArea; }
  inline Reporter reporter() { return _reporter; }

private:
  Character& _byCharacter;
  Character* _onCharacter;
  Item& _withItem;
  Area& _inArea;
  Reporter _reporter;
}; // class ItemActionExecutionArg


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
// Item Actions
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

// The ItemAction class is the abstract base class for all Item Actions.
class ItemAction
{
public:
  ItemAction(const string& name, unsigned int actionPointRequirement);
  virtual ~ItemAction();
  
  inline const string& name() const { return _name; }
  inline unsigned int actionPointRequirement() const { return _actionPointRequirement; }
  
  virtual bool canExecute(ItemActionExecutionArg& arg);
  void execute(ItemActionExecutionArg& arg);

protected:
  virtual void action(ItemActionExecutionArg& arg) = 0; // NOTE: Abstract
  virtual void willExecute(ItemActionExecutionArg& arg);
  virtual void didExecute(ItemActionExecutionArg& arg);
  
private:
  string _name;
  unsigned int _actionPointRequirement;  
}; // class ItemAction


class AttackAction : ItemAction
{
public:
  AttackAction(const string& name, unsigned int actionPointRequirement, unsigned int minimumDamage, unsigned int maximumDamage, unsigned int effectiveRange);
  virtual ~AttackAction();
  
  inline unsigned int minimumDamage() const { return _minimumDamage; }
  inline unsigned int maximumDamage() const { return _maximumDamage; }
  inline unsigned int effectiveRange() const { return _effectiveRange; }
  
  virtual bool canExecute(ItemActionExecutionArg& arg);
  
protected:
  virtual void action(ItemActionExecutionArg& arg);

private:
  unsigned int _minimumDamage;
  unsigned int _maximumDamage;
  unsigned int _effectiveRange;  
}; // class AttackAction


class AmmunitionConsumingAttackAction : AttackAction
{
public:
  AmmunitionConsumingAttackAction(const string& name, unsigned int actionPointRequirement, unsigned int minimumDamage, unsigned int maximumDamage, unsigned int effectiveRange, unsigned int ammunitionRequirement);
  virtual ~AmmunitionConsumingAttackAction();
  
  inline unsigned int ammunitionRequirement() const { return _ammunitionRequirement; }

  virtual bool canExecute(ItemActionExecutionArg& arg);

protected:
  virtual void didExecute(ItemActionExecutionArg& arg);

private:
  unsigned int _ammunitionRequirement;
}; // class AmmunitionConsumingAttackAction

} // namespace Models

#endif
