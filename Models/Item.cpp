#include <string>
#include <sstream>
#include "../Components/Point.hpp"
#include "../Exceptions/GameException.hpp"
#include "Area.hpp"
#include "Character.hpp"
#include "Item.hpp"

namespace Models
{

using std::string;

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
// Item Core
ItemCore::ItemCore(unsigned int id, const string& name, const string& description, unsigned int weight, unsigned int value)
  : _id(id), _name(name), _description(description), _weight(weight), _value(value)
{ }

ItemCore::~ItemCore()
{ }

Item::Item(ItemCore* core)
  : _core(core)
{
  if (core == nullptr)
  {
    throw Exceptions::GameException("The item core cannot be null.");
  }
}

bool operator==(const ItemCore& lhs, const ItemCore& rhs)
{
  return lhs.id() == rhs.id();
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
// Item
Item::Item(const Item& item)
  : _core(item._core)
{ }

Item::~Item()
{ }

bool Item::equals(const Item& other) const
{
  return _core == other._core;
}

bool operator==(const Item& lhs, const Item& rhs)
{
  return lhs.equals(rhs);
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
// Ammunition
Ammunition::Ammunition(ItemCore* itemCore, Type type)
  : Item(itemCore), _type(type)
{ }

Ammunition::~Ammunition()
{ }


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
// Ammunition Consuming Weapon
AmmunitionConsumingWeapon::AmmunitionConsumingWeapon(ItemCore* itemCore, Ammunition::Type ammunitionType, unsigned int ammunitionCapacity, unsigned int ammunitionCount)
  : Item(itemCore), _ammunitionType(ammunitionType),
    _ammunitionCapacity(ammunitionCapacity), _ammunitionCount(ammunitionCount)
{ }

AmmunitionConsumingWeapon::~AmmunitionConsumingWeapon()
{ }

AmmunitionConsumingWeapon& AmmunitionConsumingWeapon::useAmmunition(unsigned int amount)
{
  if (amount > ammunitionCount())
  {
    throw Exceptions::GameException("Insufficient ammunition for action.");
  }
  _ammunitionCount -= amount;
  return *this;
}

bool AmmunitionConsumingWeapon::equals(const Item& other)
{
  // Two AmmunitionConsumingWeapons are equal if they share the same ItemCore
  // and they have an equivalent ammunition count.
  if (Item::equals(other))
  {
    const AmmunitionConsumingWeapon* acw = dynamic_cast<const AmmunitionConsumingWeapon*>(&other);
  
    return acw != nullptr && acw->ammunitionCount() == ammunitionCount();
  }
  return false;
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
// Item Action Execution Arg
ItemActionExecutionArg::ItemActionExecutionArg(Character& byCharacter, Character* onCharacter, Item& withItem, Area& inArea, Reporter reporter)
  : _byCharacter(byCharacter), _onCharacter(onCharacter), _withItem(withItem), _inArea(inArea), _reporter(reporter)
{ }


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
// Item Action
ItemAction::ItemAction(const string& name, unsigned int actionPointRequirement)
  : _name(name), _actionPointRequirement(actionPointRequirement)
{ }

ItemAction::~ItemAction()
{ }

bool ItemAction::canExecute(ItemActionExecutionArg& arg)
{
  return arg.byCharacter().currentActionPoints() >= actionPointRequirement();
}

void ItemAction::execute(ItemActionExecutionArg& arg)
{
  if (canExecute(arg))
  {
    willExecute(arg);
    action(arg);
    didExecute(arg);
    arg.byCharacter().useActionPoints(actionPointRequirement());
  }
}

void ItemAction::willExecute(ItemActionExecutionArg& arg)
{ }

void ItemAction::didExecute(ItemActionExecutionArg& arg)
{ }


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
// Attack Action
AttackAction::AttackAction(const string& name, unsigned int actionPointRequirement, unsigned int minimumDamage, unsigned int maximumDamage, unsigned int effectiveRange)
  : ItemAction(name, actionPointRequirement), 
    _minimumDamage(minimumDamage), _maximumDamage(maximumDamage), _effectiveRange(effectiveRange)
{ }

AttackAction::~AttackAction()
{ }

bool AttackAction::canExecute(ItemActionExecutionArg& arg)
{
  return ItemAction::canExecute(arg) 
    && arg.onCharacter() != nullptr 
    && Components::dist(arg.byCharacter().currentPosition(), arg.onCharacter()->currentPosition()) <= effectiveRange();
}

void AttackAction::action(ItemActionExecutionArg& arg)
{
  // TODO: Do some kind of calculation to determine damage done. For now I'll
  //       just do half way between min and max.
  unsigned int dmg = (minimumDamage() + maximumDamage()) / 2;
  arg.onCharacter()->attacked(dmg);
  
  if (arg.reporter() != nullptr)
  {
    std::stringstream ss;
    ss << arg.byCharacter().name() << " attacked " << arg.onCharacter()->name() << " for " << dmg << " points of damage!";
    arg.reporter()(ss.str());
  }
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
// Ammunition Consuming Attack Action
AmmunitionConsumingAttackAction::AmmunitionConsumingAttackAction(const string& name, unsigned int actionPointRequirement, unsigned int minimumDamage, unsigned int maximumDamage, unsigned int effectiveRange, unsigned int ammunitionRequirement)
  : AttackAction(name, actionPointRequirement, minimumDamage, maximumDamage, effectiveRange),
    _ammunitionRequirement(ammunitionRequirement)
{ }

AmmunitionConsumingAttackAction::~AmmunitionConsumingAttackAction()
{ }

bool AmmunitionConsumingAttackAction::canExecute(ItemActionExecutionArg& arg)
{ 
  if (AttackAction::canExecute(arg))
  {
    AmmunitionConsumingWeapon* acw = dynamic_cast<AmmunitionConsumingWeapon*>(&arg.withItem());
    
    return acw != nullptr && acw->ammunitionCount() >= ammunitionRequirement();
  }
  return false;
}

void AmmunitionConsumingAttackAction::didExecute(ItemActionExecutionArg& arg)
{
  AmmunitionConsumingWeapon* acw = dynamic_cast<AmmunitionConsumingWeapon*>(&arg.withItem());
  
  acw->useAmmunition(ammunitionRequirement());
}

} // namespace Models
