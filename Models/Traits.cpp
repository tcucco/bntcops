#include "../Helpers/GeneralHelpers.hpp"
#include "Traits.hpp"

namespace Models
{

Traits::Traits(int strength, int perception, int endurance, int charisma, int intelligence, int agility, int luck)
{
	_strength = GetRangeBoundValue(strength);
	_perception = GetRangeBoundValue(perception);
	_endurance = GetRangeBoundValue(endurance);
	_charisma = GetRangeBoundValue(charisma);
	_intelligence = GetRangeBoundValue(intelligence);
	_agility = GetRangeBoundValue(agility);
	_luck = GetRangeBoundValue(luck);
}

// Return a value within the range inclusively defined by Traits::MinimumValue and Traits::MaximumValue.
int Traits::GetRangeBoundValue(int value)
{
  return Helpers::getRangeBoundValue(value, MinimumValue, MaximumValue);
}

} // namespace Models
