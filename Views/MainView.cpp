#include <string>
#include "MainView.hpp"
#include "../Components/Point.hpp"
#include "../Components/PositionedBox.hpp"
#include "../UI/CursesWindow.hpp"
#include "../Helpers/GeneralHelpers.hpp"
#include "../Exceptions/GameException.hpp"
#include "../Models/Area.hpp"
#include "../Models/Character.hpp"
#include "../Controllers/GameController.hpp"
#include <ncurses.h>
#include <boost/thread.hpp>
#include <boost/date_time.hpp>

namespace Views
{  

using std::string;
using UI::Curses::Window;
using Components::Point;
using Components::PositionedBox;
using Models::Area;
using Models::AreaLocation;
using Models::Character;
using Controllers::GameController;

const int KEY_ESCAPE = 27;
const int KEY_TAB = 9;
const int KEY_BKSP = 8;
const int KEY_DEL = 127;

const int MainView::CursorAttributes = A_BLINK | A_BOLD;
const int MainView::PlayerCharacterAttributes = 0;
const int MainView::NonPlayerCharacterAttributes = 0;
const int MainView::FloorAreaAttributes = 0;
const int MainView::BarrierAreaAttributes = 0;

// Public
// Constructors / Destructors
MainView::MainView(GameController& gameController)
  : _screenWidth(0), _screenHeight(0), 
    _statusWindow(nullptr), _mapWindow(nullptr), _dialogWindow(nullptr), _targetInformationWindow(nullptr), _focusedWindow(nullptr),
    _gameController(gameController), 
    _areaCursorPosition(0, 0), _mapClip(0, 0, 0, 0), _areaClip(0, 0, 0, 0)
{
  initscr();
  start_color();
  cbreak();
  noecho();
  curs_set(0);
  keypad(stdscr, TRUE);
  
  setupColors();

  getmaxyx(stdscr, _screenHeight, _screenWidth);
  
  refresh();
  
  int statusWindowWidth = _screenWidth / 5;
  int statusWindowHeight = _screenHeight;
  
  int targetInformationWindowWidth = _screenWidth / 5;
  int targetInformationWindowHeight = _screenHeight / 4;
  
  int chatWindowWidth = _screenWidth - statusWindowWidth - targetInformationWindowWidth;
  int chatWindowHeight = targetInformationWindowHeight;
    
  int mainWindowWidth = _screenWidth - statusWindowWidth;
  int mainWindowHeight = _screenHeight - chatWindowHeight;
  
  // The Status Window will sit in the top left corner of the screen.
  _statusWindow = new Window(statusWindowHeight, statusWindowWidth, 0, 0, " Status (F1) ");
  
  // The Main Window will sit just to the right of the Status Window, at the top of the screen.
  _mapWindow = new Window(mainWindowHeight, mainWindowWidth, 0, statusWindowWidth, " Area (F2) ");
  
  // The Target Information Window will sit to the right of the Status Window, below the Main Window.
  _targetInformationWindow = new Window(targetInformationWindowHeight, targetInformationWindowWidth, mainWindowHeight, statusWindowWidth, " Target ");
  
  // The Chat Window will sit to the right of the Target Information Window, below the Main Window.
  _dialogWindow = new Window(chatWindowHeight, chatWindowWidth, mainWindowHeight, statusWindowWidth + targetInformationWindowWidth, " Information ");
  _dialogWindow->isScrollable(true);
  _dialogWindow->isFocusable(false);
  
  focusedWindow(_mapWindow);
  redrawPlayerStatus();
  
  _gameController.player().propertyChanged().connect([this] (const Character& c, Models::CharacterProperty) -> void { redrawPlayerStatus(); });
  _gameController.area().characterMoved().connect([this] (const Area& a, const Character& c, const Point& from, const Point& to) -> void { onCharacterMoved(a, c, from, to); });
  
  _dialogWindow
    ->write("You have entered: ", 0, MainView::ChatWindowFirstLinePrepend, MainView::ChatWindowSubsequentLinePrepend)
    .write(_gameController.area().name(), 0, MainView::ChatWindowFirstLinePrepend, MainView::ChatWindowSubsequentLinePrepend)
    .lineFeed()
    .write(_gameController.area().description(), 0, MainView::ChatWindowFirstLinePrepend, MainView::ChatWindowSubsequentLinePrepend);
  
  setAreaCursorPosition(_gameController.player().currentPosition());
  redrawTargetInformation();
  refreshView();
}

MainView::~MainView()
{
  delete _statusWindow;
  delete _mapWindow;
  delete _targetInformationWindow;
  delete _dialogWindow;
  endwin();
}

// Setters
bool MainView::focusedWindow(Window* window)
{
  if (window == 0 || window->isFocusable())
  {
    if (_focusedWindow != window)
    {
      if (_focusedWindow != 0)
      {
        _focusedWindow->isFocused(false);
      }
      _focusedWindow = window;
      if (_focusedWindow != 0)
      {
        _focusedWindow->isFocused(true);
      }
    }
    return true;
  }
  return false;
}

// Methods
void MainView::commandLoop()
{
  int jump = 1;
  string str;
  int ch;
  
  while((ch = getch()) != KEY_ESCAPE)
  {
    // For input testing
    // _dialogWindow->writeLine(Helpers::toString(ch), "> ");
    
    // TODO: I need to set this up so that certain keys go to the top window
    //       and the remainder of the keys are routed to a special handler
    //       for the focused window.
    bool update = true;
    
    switch(ch)
    {
      case KEY_F(1):
        focusedWindow(_statusWindow);
        break;
      case KEY_F(2):
        focusedWindow(_mapWindow);
        break;
      case KEY_RIGHT:
        setAreaCursorPosition(Point(_areaCursorPosition.x() + jump, _areaCursorPosition.y()));
        break;
      case KEY_UP:
        setAreaCursorPosition(Point(_areaCursorPosition.x(), _areaCursorPosition.y() - jump));
        break;
      case KEY_LEFT:
        setAreaCursorPosition(Point(_areaCursorPosition.x() - jump, _areaCursorPosition.y()));
        break;
      case KEY_DOWN:
        setAreaCursorPosition(Point(_areaCursorPosition.x(), _areaCursorPosition.y() + jump));
        break;
      case KEY_TAB:
        jump = jump == 1 ? 10 : 1;
        update = false;
        break;
      case 'c':
        // Center the cursor on the player.
        setAreaCursorPosition(_gameController.player().currentPosition(), true);
        break;
      case 'm':
        // Move the player to the center of the selected location.
        _gameController.area().moveCharacter(_gameController.player(), Components::floor(_areaCursorPosition) + Point(0.5, 0.5));
        update = false;
        break;
    }
    
    if (update) 
    { 
      refreshView(); 
    }
  }
}

// Private
// Setters
void MainView::setAreaCursorPosition(Point p, bool forceRefresh)
{
  int x = Helpers::getRangeBoundValue((int)p.x(), 0, _gameController.area().width() - 1);
  int y = Helpers::getRangeBoundValue((int)p.y(), 0, _gameController.area().height() - 1);
  p = Point(x, y);
  
  if (_areaCursorPosition != p)
  {
    if (forceRefresh || !_areaClip.containsPoint(p))
    {
      _areaCursorPosition = p;
      redrawMap();
    }
    else
    {
      Point oldPoint = _areaCursorPosition;
      _areaCursorPosition = p;
      drawMapPoint(oldPoint);
      drawMapPoint(_areaCursorPosition);
    }
    redrawTargetInformation();
  }
}

// Player Status Window Methods
void MainView::redrawPlayerStatus()
{
  Window& w = *_statusWindow;
  Character& pc = _gameController.player();
  
  int headingAttrs = A_BOLD | A_UNDERLINE;
  int keyAttrs = A_BOLD;
  int valAttrs = 0;
  
  int hpColor = getColorForPercentage((double)pc.currentHitPoints() / pc.maximumHitPoints());
  int apColor = getColorForPercentage((double)pc.currentActionPoints() / pc.maximumActionPoints());
  int wgtColor = getColorForPercentage((double)pc.currentCarryWeight() / pc.maximumCarryWeight(), false);
  
  double maxTrait = Models::Traits::MaximumValue;
  double traitPt1 = 0.4;
  double traitPt2 = 0.8;  
  int strColor = getColorForPercentage(pc.traits().strength() / maxTrait, true, traitPt1, traitPt2);
  int perColor = getColorForPercentage(pc.traits().perception() / maxTrait, true, traitPt1, traitPt2);
  int endColor = getColorForPercentage(pc.traits().endurance() / maxTrait, true, traitPt1, traitPt2);
  int chaColor = getColorForPercentage(pc.traits().charisma() / maxTrait, true, traitPt1, traitPt2);
  int intColor = getColorForPercentage(pc.traits().intelligence() / maxTrait, true, traitPt1, traitPt2);
  int agiColor = getColorForPercentage(pc.traits().agility() / maxTrait, true, traitPt1, traitPt2);
  int lckColor = getColorForPercentage(pc.traits().luck() / maxTrait, true, traitPt1, traitPt2);
  
  w.clear()
    .write("General", headingAttrs)
    .lineFeed()
    
    .write("Name: ", keyAttrs)
    .write(pc.name(), valAttrs)
    .lineFeed()
    
    .write("  HP: ", keyAttrs)
    .write(Helpers::toString(pc.currentHitPoints()), valAttrs | hpColor)
    .write(" / ", valAttrs)
    .write(Helpers::toString(pc.maximumHitPoints()), valAttrs)
    .lineFeed()
    
    .write("  AP: ", keyAttrs)
    .write(Helpers::toString(pc.currentActionPoints()), valAttrs | apColor)
    .write(" / ", valAttrs)
    .write(Helpers::toString(pc.maximumActionPoints()), valAttrs)
    .lineFeed()
    
    .write(" Wgt: ", keyAttrs)
    .write(Helpers::toString(pc.currentCarryWeight()), valAttrs | wgtColor)
    .write(" / ", valAttrs)
    .write(Helpers::toString(pc.maximumCarryWeight()), valAttrs)
    .lineFeed()
    .lineFeed()
    
    .write("Traits", headingAttrs)
    .lineFeed()
    .write(" STR: ", keyAttrs)
    .write(Helpers::toString(pc.traits().strength()), valAttrs | strColor)
    .lineFeed()
    .write(" PER: ", keyAttrs)
    .write(Helpers::toString(pc.traits().perception()), valAttrs | perColor)
    .lineFeed()
    .write(" END: ", keyAttrs)
    .write(Helpers::toString(pc.traits().endurance()), valAttrs | endColor)
    .lineFeed()
    .write(" CHA: ", keyAttrs)
    .write(Helpers::toString(pc.traits().charisma()), valAttrs | chaColor)
    .lineFeed()
    .write(" INT: ", keyAttrs)
    .write(Helpers::toString(pc.traits().intelligence()), valAttrs | intColor)
    .lineFeed()
    .write(" AGI: ", keyAttrs)
    .write(Helpers::toString(pc.traits().agility()), valAttrs | agiColor)
    .lineFeed()
    .write(" LCK: ", keyAttrs)
    .write(Helpers::toString(pc.traits().luck()), valAttrs | lckColor)
    .lineFeed();
}

void MainView::drawMapPoint(const Point& point, MainView::PointRelativeTo relativeTo)
{
  Point screenPosition;
  Point areaPosition;
  
  switch (relativeTo)
  {
    case MainView::PointRelativeTo::Window:
      screenPosition = point;
      areaPosition = point - _mapClip.topLeft() + _areaClip.topLeft();
      break;
    case MainView::PointRelativeTo::MapClip:
      screenPosition = point + _mapClip.topLeft();
      areaPosition = point + _areaClip.topLeft();
      break;
    case MainView::PointRelativeTo::Area:
      screenPosition = point - _areaClip.topLeft() + _mapClip.topLeft();
      areaPosition = point;
      break;
  }

  if (!_areaClip.containsPoint(areaPosition)) 
  { 
    return; 
  }
  
  AreaLocation& l = _gameController.area().location(areaPosition);
  int additionalCursorAttributes = 0;
  
  if (l.occupyingCharacter() != 0)
  {
    additionalCursorAttributes |= A_UNDERLINE;
  }
  
  if (Components::floor(areaPosition) == Components::floor(_areaCursorPosition))
  {
    _mapWindow->putChar(MainView::CursorSymbol, screenPosition.y(), screenPosition.x(), COLOR_PAIR(MainView::CursorColorId) | MainView::CursorAttributes | additionalCursorAttributes);
  }
  else
  {
    if (l.occupyingCharacter() != 0)
    {
      if (*l.occupyingCharacter() == _gameController.player())
      {
        _mapWindow->putChar(MainView::PlayerCharacterSymbol, screenPosition.y(), screenPosition.x(), COLOR_PAIR(MainView::PlayerCharacterColorId) | MainView::PlayerCharacterAttributes);
      }
      else
      {
        _mapWindow->putChar(MainView::NonPlayerCharacterSymbol, screenPosition.y(), screenPosition.x(), COLOR_PAIR(MainView::NonPlayerCharacterColorId) | MainView::NonPlayerCharacterAttributes);
      }
    }
    else
    {
      using Exceptions::GameException;
      
      int symbol;
      int attributes;
      
      switch (l.locationType())
      {
        case Models::AreaLocationType::Floor:
          symbol = MainView::FloorAreaSymbol;
          attributes = COLOR_PAIR(MainView::FloorAreaColorId) | MainView::FloorAreaAttributes;
          break;
        case Models::AreaLocationType::Barrier:
          symbol = MainView::BarrierAreaSymbol;
          attributes = COLOR_PAIR(MainView::BarrierAreaColorId) | MainView::BarrierAreaAttributes;
          break;
        case Models::AreaLocationType::Exit:
          throw GameException("You haven't yet implemented how the view will render exit locations yet.");
          break;
        default:
          throw GameException("Unknown area location type.");
          break;
      }
      
      _mapWindow->putChar(symbol, screenPosition.y(), screenPosition.x(), attributes);
    }
  }
}

void MainView::redrawMap()
{
  // Area clip will be at largest the dimensions of the main window.
  _areaClip = getAreaClip(_gameController.area(), Box(_mapWindow->innerWidth(), _mapWindow->innerHeight()), _areaCursorPosition);
  
  // The mapClip will be positioned so that it is in the center of the screen.
  int mapX = (_mapWindow->innerWidth() - (int)_areaClip.width()) / 2;
  int mapY = (_mapWindow->innerHeight() - (int)_areaClip.height()) / 2;
  
  // Create a box with the same size as the area clip, but set in such a way as
  // to center the drawing on the map window in the case that the area map is 
  // smaller than the window.
  _mapClip = PositionedBox(_areaClip, mapX, mapY);
 
  // First, draw any padding that is necessary in the map window. This is done
  // when the area clip width is less than the window width, or the area cip
  // height is less than the screen height. It is determined by the mapClip's
  // top left corner. Anything other than (0, 0) means that the mapClip is
  // offset from the top left of the window.
  int lp = _mapClip.topLeft().x();
  int tp = _mapClip.topLeft().y();
  
  if (lp > 0)
  {
    for (unsigned int r = 0; r < _mapWindow->innerHeight(); ++r)
    {
      for (int c = 0; c < lp; ++c)
      {
        _mapWindow->putChar(MainView::OutsideAreaSymbol, r, c, 0);
      }
      for (unsigned int c = lp + _areaClip.width(); c < _mapWindow->innerWidth(); ++c)
      {
        _mapWindow->putChar(MainView::OutsideAreaSymbol, r, c, 0);
      }
    }
  }
  
  if (tp > 0)
  {
    for (unsigned int c = 0; c < _mapWindow->innerWidth(); ++c)
    {
      for (int r = 0; r < tp; ++r)
      {
        _mapWindow->putChar(MainView::OutsideAreaSymbol, r, c, 0);
      }
      for (unsigned int r = tp + _areaClip.height(); r < _mapWindow->innerHeight(); ++r)
      {
        _mapWindow->putChar(MainView::OutsideAreaSymbol, r, c, 0);
      }
    }
  }
  
  for (int r = 0; r < (int)_mapClip.height(); ++r)
  {
    for (int c = 0; c < (int)_mapClip.width(); ++c)
    {
      drawMapPoint(Point(c, r), MainView::PointRelativeTo::MapClip);
    }
  }
}

// Target Information Window Methods
void MainView::redrawTargetInformation()
{
  // TODO: Update this so that instead of rewriting the entire window each
  //       time it knows where each piece of information goes and just updates
  //       that. I think that's what's causing the screen flash from time to
  //       time.
  int keyAttrs = A_BOLD;
  int valAttrs = 0;
  AreaLocation& l = _gameController.area().location(_areaCursorPosition);
  Window& w = *_targetInformationWindow;
  
  w.clear()
    .write("Pos:", keyAttrs)
    .write(" (", valAttrs)
    .write(Helpers::toString((int)_areaCursorPosition.x()), valAttrs)
    .write(", ", valAttrs)
    .write(Helpers::toString((int)_areaCursorPosition.y()), valAttrs)
    .write(")")
    .lineFeed()

    .write("Distance: ", keyAttrs)
    .write(Helpers::toString((int)dist(_areaCursorPosition, _gameController.player().currentPosition())))
    .lineFeed()

    .write("Type: ", keyAttrs)
    .write(toString(l.locationType()), valAttrs)
    .lineFeed();
  
  if (l.occupyingCharacter() != 0)
  {
    w.write("Occupied By: ", keyAttrs)
      .write(l.occupyingCharacter()->name());
    
    if (*l.occupyingCharacter() == _gameController.player())
    {
      w.write(" (you)");
    }
    else
    {
      w.lineFeed()
        .write(getPronoun(l.occupyingCharacter()->gender(), true))
        .write(" looks ")
        .write(getCharacterDescription(*l.occupyingCharacter()));
    }
    w.lineFeed();
  }
}

// Signal Handlers
void MainView::onCharacterMoved(const Area& area, const Character& character, const Point& fromAreaPosition, const Point& toAreaPosition)
{
  if (Components::floor(fromAreaPosition) != Components::floor(toAreaPosition))
  {
    drawMapPoint(fromAreaPosition);
    drawMapPoint(toAreaPosition);
    redrawTargetInformation();
    refreshView();
    
    // My expectation is that this will typically be called in succession, so
    // this pause will cause the screen to update more slowly so the character
    // appears to walk across the string.
    // std::this_thread::sleep_for(std::chrono::milliseconds(250));
    boost::this_thread::sleep(boost::posix_time::milliseconds(250));
  }
  else
  {
    redrawTargetInformation();
    refreshView();
  }
}

void MainView::writeToDialog(const string& message)
{
  _dialogWindow->writeLine(message, MainView::ChatWindowFirstLinePrepend, MainView::ChatWindowSubsequentLinePrepend);
}
  
void MainView::refreshView()
{
  _statusWindow->prepareForUpdate();
  _mapWindow->prepareForUpdate();
  _targetInformationWindow->prepareForUpdate();
  _dialogWindow->prepareForUpdate();
  doupdate();
}
  
// Setup Methods
void MainView::setupColors()
{
  int defaultBackground = COLOR_BLACK;
  int defaultForeground = COLOR_WHITE;
  
  init_pair(Window::HighlightTitleColorId, COLOR_CYAN, defaultBackground);
  init_pair(MainView::GoodColorId, COLOR_GREEN, defaultBackground);
  init_pair(MainView::BadColorId, COLOR_RED, defaultBackground);
  init_pair(MainView::NeutralColorId, defaultForeground, defaultBackground);
  init_pair(MainView::WarningColorId, COLOR_YELLOW, defaultBackground);
  init_pair(MainView::CursorColorId, COLOR_YELLOW, defaultBackground);
  init_pair(MainView::PlayerCharacterColorId, COLOR_CYAN, defaultBackground);
  init_pair(MainView::NonPlayerCharacterColorId, COLOR_RED, defaultBackground);
  init_pair(MainView::FloorAreaColorId, defaultForeground, defaultBackground);
  init_pair(MainView::BarrierAreaColorId, defaultForeground, defaultBackground);
}

// Namespace Methods
// Helper Methods
int getColorForPercentage(double percentage, bool higherIsBetter, double pt1, double pt2)
{
  if (!higherIsBetter)
  {
    percentage = 1 - percentage;
  }
  
  if (percentage < pt1)
  {
    return COLOR_PAIR(MainView::BadColorId);
  }
  if (percentage < pt2)
  {
    return COLOR_PAIR(MainView::NeutralColorId);
  }
  else
  {
    return COLOR_PAIR(MainView::GoodColorId);
  }
}

string getCharacterDescription(const Character& character)
{
  double hpPct = character.currentHitPoints() / character.maximumHitPoints();
  
  if (hpPct == 1.0)
  {
    return "fine";
  }
  else if (hpPct > 0.75)
  {
    return "lightly wounded";
  }
  else if (hpPct > 0.50)
  {
    return "wounded";
  }
  else if (hpPct > 0.25)
  {
    return "severly wounded";
  }
  else
  {
    return "almost dead";
  }
}

PositionedBox getAreaClip(const Area& forArea, const Box& clipSize, const Point& containingPoint)
{
  // Horizontal Calculation.
  // First, determine our minX and maxX
  int minX = containingPoint.x() - clipSize.width() / 2;
  int maxX = minX + clipSize.width();
  
  // Now fit the horizontal dimensions to the area size, if necessary.
  if (minX < 0)
  {
    // If the left point is outside the area, slide the left and right points
    // over so that the left is at zero.
    maxX += -minX;
    minX = 0;
   
    // Now, check to see if the right side is hanging off the edge of the area.
    // If so, slide it back to the edge. It's not necessary to alter the left
    // side, because we've already set it to the edge. In this case, the clip
    // area's width will be smaller than requested, because the area is smaller.
    if (maxX > forArea.width())
    {
      maxX = forArea.width();
    }
  }
  else if (maxX > forArea.width())
  {
    // If the right point is outside the area, slide the left and right points
    // over so that the right is at the edge of the area.
    minX -= (maxX - forArea.width());
    maxX = forArea.width();
    
    // Now, check to see if the left side is hanging off the edge of the area.
    // If so, slide it back to the edge. It's not necessary to alter the right
    // side, because we've already set it to the edge. In this case, the clip
    // area's width will be smaller than requested, because the area is smaller.
    if (minX < 0)
    {
      minX = 0;
    }
  }
  
  // Vertical Calculation.
  // All the logic here is similar to the vertical case above. See the comments
  // there to understand what is going on here.
  int minY = containingPoint.y() - clipSize.height() / 2;
  int maxY = minY + clipSize.height();
  
  if (minY < 0)
  {
    maxY += -minY;
    minY = 0;
    
    if (maxY > forArea.height())
    {
      maxY = forArea.height();
    }
  }
  else if (maxY > forArea.height())
  {
    minY -= (maxY - forArea.height());
    maxY = forArea.height();
    
    if (minY < 0)
    {
      minY = 0;
    }
  }
  
  // At this point, we have the dimensions we need to determine the size and
  // location of the clip of the area.
  return PositionedBox(maxX - minX, maxY - minY, minX, minY);
}

} // namespace Views
