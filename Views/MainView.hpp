#ifndef VIEWS_MAIN_VIEW_HPP
#define VIEWS_MAIN_VIEW_HPP

#include <string>
#include "../Components/Point.hpp"
#include "../Components/Box.hpp"
#include "../Components/PositionedBox.hpp"
#include "../UI/CursesWindow.hpp"
#include "../Models/Area.hpp"
#include "../Models/Character.hpp"
#include "../Controllers/GameController.hpp"

namespace Views
{

using std::string;
using UI::Curses::Window;
using Components::Point;
using Components::Box;
using Components::PositionedBox;
using Models::Area;
using Models::Character;
using Controllers::GameController;

class MainView
{
// Object Members
public:
  explicit MainView(GameController& controller);
  ~MainView();

  inline Window* focusedWindow() const { return _focusedWindow; }
  bool focusedWindow(Window* window);
  
  void commandLoop();
  
private:
  enum class PointRelativeTo { Area, MapClip, Window };
    
  int _screenWidth;
  int _screenHeight;

  Window* _statusWindow;  
  Window* _mapWindow;
  Window* _dialogWindow;
  Window* _targetInformationWindow;
  Window* _focusedWindow;
  
  GameController& _gameController;
  Point _areaCursorPosition;
  // This contains the section of the map window that the area will be printed in
  PositionedBox _mapClip;
  // This contains the section of the area that will be printed.
  PositionedBox _areaClip;
  
  // Setters
  // void mapCursorPosition(Point p);  
  void setAreaCursorPosition(Point p, bool forceRefresh = false);
  
  void redrawPlayerStatus();
  void drawMapPoint(const Point& point, MainView::PointRelativeTo relativeTo = MainView::PointRelativeTo::Area);
  void redrawMap();
  void redrawTargetInformation();
  void writeToDialog(const string& message);
  
  void refreshView();
  
  // Signal Handlers
  void onCharacterMoved(const Area& area, const Character& character, const Point& fromAreaPosition, const Point& toAreaPosition);

  // Setup Methods
  void setupColors();

// TODO: These really should all be moved to cpp. It's implementation details.
// Class Members
public:
  constexpr static const char* ChatWindowFirstLinePrepend = "> ";
  constexpr static const char* ChatWindowSubsequentLinePrepend = "  ";
  
  // Color IDs
  static const int GoodColorId = 20;
  static const int BadColorId = 21;
  static const int NeutralColorId = 22;
  static const int WarningColorId = 23;
  static const int CursorColorId = 24;
  static const int PlayerCharacterColorId = 25;
  static const int NonPlayerCharacterColorId = 26;
  static const int FloorAreaColorId = 27;
  static const int BarrierAreaColorId = 28;
  
  // Map Symbols
  static const int CursorSymbol = 'X';
  static const int PlayerCharacterSymbol = 'U';
  static const int NonPlayerCharacterSymbol = 'E';
  static const int OutsideAreaSymbol = '-';
  static const int FloorAreaSymbol = ' ';
  static const int BarrierAreaSymbol = 'b';
  
  // Attributes
  static const int CursorAttributes;
  static const int PlayerCharacterAttributes;
  static const int NonPlayerCharacterAttributes;
  static const int FloorAreaAttributes;
  static const int BarrierAreaAttributes;
}; // class MainView

// Helper Methods
int getColorForPercentage(double percentage, bool higherIsBetter = true, double pt1 = 0.25, double pt2 = 0.75);
string getCharacterDescription(const Character& character);
PositionedBox getAreaClip(const Area& forArea, const Box& clipSize, const Point& containingPoint);

} // namespace Views

#endif // VIEWS_MAIN_VIEW_HPP
